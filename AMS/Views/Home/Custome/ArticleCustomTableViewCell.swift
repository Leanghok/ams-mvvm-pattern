//
//  ArticleCustomTableViewCell.swift
//  AMS
//
//  Created by Hour Leanghok on 12/17/18.
//  Copyright © 2018 Hour Leanghok. All rights reserved.
//

import UIKit

class ArticleCustomTableViewCell: UITableViewCell {
    
    //MARK: Outlets
    @IBOutlet weak var articleImageView: UIImageView!
    
    @IBOutlet weak var articleTitleLabel: UILabel!
    
    @IBOutlet weak var articleDescriptionLabel: UILabel!
    
    @IBOutlet weak var articlePublishDateLabel: UILabel!
    
    @IBOutlet weak var articleAuthorLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
