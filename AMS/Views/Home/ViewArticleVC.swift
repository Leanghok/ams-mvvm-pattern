//
//  ViewArticleVC.swift
//  AMS
//
//  Created by Hour Leanghok on 12/21/18.
//  Copyright © 2018 Hour Leanghok. All rights reserved.
//

import UIKit

class ViewArticleVC: UIViewController {

    //MARK: Outlets
    
    @IBOutlet weak var articleImageView: UIImageView!
    
    @IBOutlet weak var articleTitleLabel: UILabel!
    
    @IBOutlet weak var articleDescriptionLabel: UILabel!
    
    //Mark: Local Variable
    var articleImage = UIImage(named: "no_image_available")
    var articleTitle = ""
    var articleDescription = ""
    
    
    override func viewDidLoad() {
        articleTitleLabel.text = articleTitle
        articleImageView.image = articleImage
        articleDescriptionLabel.text = articleDescription
        super.viewDidLoad()
    }
    
}
