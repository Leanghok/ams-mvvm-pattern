//
//  UpdateArticleViewModel.swift
//  AMS
//
//  Created by Hour Leanghok on 12/22/18.
//  Copyright © 2018 Hour Leanghok. All rights reserved.
//

import Foundation
import UIKit
class UpdateArticleViewModel{
    
    func updateArticle(id: Int,title:String, description:String, image: UIImage?, completionHandler: @escaping (CompletionHandler<String>) -> Void){
        
        DataAccess.shared.updateArticle(title: title, description: description, image: image, url: API.ARTICLE_UPDATE(id: id)) { (response) in
            switch response {
            case .success(let message):
                completionHandler(.success(message))
            case .failure(let message):
                completionHandler(.failure(message))
            }
        }
    }
}


