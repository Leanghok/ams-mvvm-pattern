//
//  AddArticleViewModel.swift
//  AMS
//
//  Created by Hour Leanghok on 12/21/18.
//  Copyright © 2018 Hour Leanghok. All rights reserved.
//

import Foundation
import UIKit

class AddArticleViewModel{
    func uploadArticle(title: String, description: String, image: UIImage, completionHandler: @escaping (CompletionHandler<String>) -> Void){
        DataAccess.shared.uploadArticle(url: API.POSTArticle, title: title, description: description, image: image) { (response) in
            switch response{
            case .success(let response):
                completionHandler(.success(response))
            case .failure(let response):
                completionHandler(.failure(response))
            }
        }
    }
    
    
}


