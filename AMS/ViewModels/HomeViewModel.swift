//
//  HomeViewModel.swift
//  AMS
//
//  Created by Hour Leanghok on 12/17/18.
//  Copyright © 2018 Hour Leanghok. All rights reserved.
//

import Foundation

class HomeViewModel{
    
    var articles = [Article]()
    
    func fetchArticles(page: Int,completion: @escaping (String?) -> Void){
        DataAccess.shared.fetch(url: API.ARTICLE_LIST(page: page), responseType: JSONArticleResponse.self) { (result) in
            switch result{
            case .success(let response):
                if page == 1 {
                    self.articles = response.articles!
                    completion(nil)
                }else{
                    self.articles += response.articles!
                    completion(nil)
                }
            case .failure(let error):
                completion(error)
            }
        }
    }
    
}
