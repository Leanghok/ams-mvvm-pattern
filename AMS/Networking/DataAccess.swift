//
//  DataAccess.swift
//  AMS
//
//  Created by Hour Leanghok on 12/17/18.
//  Copyright © 2018 Hour Leanghok. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
}

enum CompletionHandler<T: Codable> {
    case success(T)
    case failure(String)
}

class DataAccess {
    
    static let shared = DataAccess()
    
    private init() { }
    
    //TO DO: Fetch Data
    func fetch<T>(url: URL, responseType: T.Type, completion: @escaping (CompletionHandler<T>) -> Void){
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.get.rawValue
        URLSession.shared.dataTask(with: request) { (data, response, error) in

            guard error == nil else {
                print("Error")
                completion(.failure("Error"))
                return
            }

            guard let data = data else {
                print("Data is null")
                return
            }
            
            do {
                let result = try JSONDecoder().decode(T.self, from: data)
                print("Success")
                completion(.success(result))
            }catch let err{
                print(err)
            }
            
        }.resume()
    }
    
    
    //TO DO: Post Image
    func uploadImage(url: URL,image: UIImage ,handleComplete: @escaping ((String) -> Void)) {
        let header = ["Authorization": "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ=", "Content-Type": "application/json", "Accept": "application/json"]
        Alamofire.upload(multipartFormData: { (multipart) in
            multipart.append(image.jpegData(compressionQuality: 0.2)!, withName: "FILE", fileName: ".jpg", mimeType: "image/jpeg")
        }, to: url, method: .post, headers: header) { (result) in
            switch result{
            case .success(request: let upload, _ , _):
                upload.responseData(completionHandler: { (response) in
                    let data = try? JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as! [String:Any]
//                    imageData = data!["DATA"] as? String
                    handleComplete(data!["DATA"] as! String)
                })
            case .failure(let error):
                print(error)
            }
        }
        return
    }
    
    //TO DO: Post Article
    func uploadArticle(url: URL,title:String, description: String, image: UIImage, completion: @escaping (CompletionHandler<String>) -> Void){
        uploadImage(url: API.UPLOAD_FILE, image: image) { (imageUrl) in
            let params: Parameters = [
                "TITLE": title,
                "DESCRIPTION": description,
                "IMAGE": imageUrl
            ]
            let headers: HTTPHeaders = [
                "Authorization": "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ=",
                "Accept": "application/json"
            ]
            Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseData { (response) in
                if response.result.isSuccess {
                    completion(.success("Success"))
                }else{
                    completion(.failure("Failed to post"))
                }
            }
        }
        
    }
    
    //TO DO: Delete Article
    func deleteArticle(url: URL, completionHandler: @escaping (CompletionHandler<String>) -> Void){
        Alamofire.request(url, method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            if response.result.isSuccess{
                completionHandler(.success("Deleted"))
            }else{
                completionHandler(.failure("Can't be deleted"))
            }
        }
    }
    
    //TO DO: Update Article
    func updateArticle(title:String, description: String, image: UIImage?,url: URL, completionHandler: @escaping (CompletionHandler<String>) -> Void){
        let header = ["Authorization": "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ=", "Content-Type": "application/json", "Accept": "application/json"]
        
        var params:Parameters = [:]
        print("Updated Article Run")
        if image == nil {
            print("Updated Article image is nil")
            params = [
                "TITLE": title,
                "DESCRIPTION": description
            ]
            Alamofire.request(url, method: .put, parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                if response.result.isSuccess {
                    completionHandler(.success("Updated"))
                }else{
                    print("Updated Article Run \(response.description)")
                }
            }
        }else{
            self.uploadImage(url: API.UPLOAD_FILE, image: image!) { (imageUrl) in
                params = [
                    "TITLE": title,
                    "DESCRIPTION": description,
                    "IMAGE": imageUrl
                ]
                Alamofire.request(url, method: .put, parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                    if response.result.isSuccess {
                        completionHandler(.success("Updated"))
                    }
                }
            }
        }
    }
    
    
    
}
